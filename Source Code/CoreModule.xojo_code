#tag Module
Protected Module CoreModule
	#tag Method, Flags = &h0
		Sub ConfigLoad()
		  Dim xml as New XmlDocument
		  Dim nodes as XmlNodeList
		  Dim node as XmlNode
		  Dim f as FolderItem
		  f=New FolderItem("config.xml")
		  if f<>nil then
		    if f.exists then
		      xml.LoadXml(f)
		      nodes=xml.XQL("//Set")
		      
		      For i as Integer = 0 To nodes.Length-1
		        node=nodes.Item(i)
		        //msgbox(node.GetAttribute("name")+": "+node.GetAttribute("value"))
		        
		        Select Case node.GetAttribute("name")
		        Case "kmlpath"
		          MainWindow.kmlpath.Text=node.GetAttribute("value")
		          
		        Case "photopath"
		          MainWindow.photopath.Text=node.GetAttribute("value")
		          
		        Case "exportpath"
		          MainWindow.exportpath.Text=node.GetAttribute("value")
		          
		        Case "Tolerance"
		          MainWindow.Tolerance.Text=node.GetAttribute("value")
		          
		        Case "eNBLCIDDirMake"
		          if node.GetAttribute("value") = "False" then
		            MainWindow.eNBLCIDDirMake.Value = False
		          else
		            MainWindow.eNBLCIDDirMake.Value = True
		          end if
		          
		        Case "eNBLCIDDirNameNotComma"
		          if node.GetAttribute("value") = "False" then
		            MainWindow.eNBLCIDDirNameNotComma.Value = False
		          else
		            MainWindow.eNBLCIDDirNameNotComma.Value = True
		          end if
		          
		        Case "ChangeFileName"
		          if node.GetAttribute("value") = "False" then
		            MainWindow.ChangeFileName.Value = False
		          else
		            MainWindow.ChangeFileName.Value = True
		          end if
		          
		        Case "FileNameFormat"
		          MainWindow.FileNameFormat.Text=node.GetAttribute("value")
		          
		        end Select
		        
		      Next
		      
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ConfigSave()
		  Dim xml as New XmlDocument
		  Dim ParentNode,ValueNode as XmlNode
		  Dim f as FolderItem
		  f=New FolderItem("config.xml")
		  if f<>nil then
		    ParentNode=xml.AppendChild(xml.CreateElement("config"))
		    ParentNode.SetAttribute("formatversion", "1")
		    
		    //KMLpath
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "kmlpath")
		    ValueNode.SetAttribute("value", MainWindow.kmlpath.Text)
		    
		    //PhotoPath
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "photopath")
		    ValueNode.SetAttribute("value", MainWindow.photopath.Text)
		    
		    //ExportPath
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "exportpath")
		    ValueNode.SetAttribute("value", MainWindow.exportpath.Text)
		    
		    //Tolerance
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "Tolerance")
		    ValueNode.SetAttribute("value", MainWindow.Tolerance.Text)
		    
		    //eNBLCIDDirMake
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "eNBLCIDDirMake")
		    ValueNode.SetAttribute("value", MainWindow.eNBLCIDDirMake.Value.ToString)
		    
		    //eNBLCIDDirNameNotComma
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "eNBLCIDDirNameNotComma")
		    ValueNode.SetAttribute("value", MainWindow.eNBLCIDDirNameNotComma.Value.ToString)
		    
		    //ChangeFileName
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "ChangeFileName")
		    ValueNode.SetAttribute("value", MainWindow.ChangeFileName.Value.ToString)
		    
		    //FileNameFormat
		    ValueNode=ParentNode.AppendChild(xml.CreateElement("Set"))
		    ValueNode.SetAttribute("name", "FileNameFormat")
		    ValueNode.SetAttribute("value", MainWindow.FileNameFormat.Text)
		    
		    xml.SaveXml(f)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetMetaData_for_macOS(source as FolderItem, byref DateTIme as String, byref Latitude as String, byref Longitude as String)
		  Var desc As String
		  
		  Dim SearchObject as New RegEx
		  Dim MatchObject as RegExMatch
		  
		  #If TargetMacOS
		    
		    // Declares against the "Foundation" framework
		    Declare Function NSClassFromString Lib "Foundation" (clsName As CFStringRef) As Ptr
		    Declare Function FileURLWithPath Lib "Foundation" Selector "fileURLWithPath:" (obj As Ptr, path As CFStringRef) As Ptr
		    Declare Function Description Lib "Foundation" Selector "description" (dict As Ptr) As CFStringRef
		    
		    // Declares against the "ImageIO" framework
		    Declare Function CGImageSourceCreateWithURL Lib "ImageIO" (path As Ptr, options As Ptr) As Ptr
		    Declare Function CGImageSourceCopyPropertiesAtIndex Lib "ImageIO" (imageSource As Ptr, index As Integer, options As Ptr) As Ptr
		    
		    If source <> Nil Then
		      
		      // Getting a Reference to the NSURL class
		      Var nsurl As Ptr = NSClassFromString("NSURL")
		      
		      // Getting a reference to a NSURL object created from the given Path
		      // for the source file
		      Var filePath As Ptr = FileURLWithPath(nsurl, source.NativePath)
		      
		      // Getting a reference to the image object created from the 
		      // given NSURL object
		      Var imageRef As Ptr = CGImageSourceCreateWithURL(filePath, Nil)
		      
		      // Getting an NSDictionary containing the Metadata for the image
		      Var dict As Ptr = CGImageSourceCopyPropertiesAtIndex(imageRef, 0, Nil)
		      
		      // Getting the contents of the NSDictionary instance as a String
		      // This will contain the metadata for the image file!
		      desc = Description(dict)
		      
		    End If
		    
		  #EndIf
		  
		  //DateTimeOriginal = "2022:06:19 17:56:12";
		  SearchObject.SearchPattern="DateTimeOriginal = ""(.*)"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    DateTIme=MatchObject.SubExpressionString(1)
		  end if
		  
		  //Latitude = "34.99706666666667";
		  SearchObject.SearchPattern="Latitude = ""(.*)"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    Latitude=MatchObject.SubExpressionString(1)
		  end if
		  
		  //Longitude = "137.0679333333333";
		  SearchObject.SearchPattern="Longitude = ""(.*)"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    Longitude=MatchObject.SubExpressionString(1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadKML(kmlpath as String)
		  Dim sourcef as FolderItem
		  Dim kml as new XmlDocument
		  
		  Dim nodes as XmlNodeList
		  Dim node as XmlNode
		  Dim placemarkchild,pointchild as XmlNode
		  
		  Dim SearchPlacemark as New RegEx
		  Dim MatchPlacemark as RegExMatch
		  Dim Searchcoordinates as New RegEx
		  Dim Matchcoordinates as RegExMatch
		  Dim CurrentENBLCID,CurrentLongitude,CurrentLatitude as String
		  SearchPlacemark.SearchPattern="\d{5,6}-(\d{1,2})(,\d{1,2})?(,\d{1,2})?"
		  Searchcoordinates.SearchPattern="(\d+\.\d+),(\d+\.\d+),\d"
		  
		  Dim DBFile as FolderItem = New FolderItem("Picturedb.sqlite")
		  Dim dbrow as New DatabaseRow
		  Dim sql as String
		  //既存のSQLiteデータベースがあったらバーーーーーーン
		  if dbFile <> Nil then
		    if dbFile.Exists then
		      dbFile.Remove
		    end if
		  end if
		  //SQLiteデータベースファイルを新規作成
		  App.DBFile = New SQLiteDatabase
		  App.DBFile.DatabaseFile = DBFile
		  App.DBFile.CreateDatabase
		  sql = "CREATE TABLE DataTable (""eNB-LCID"" TEXT, ""latitude"" TEXT, ""longitude"" TEXT);"
		  App.DBFile.ExecuteSQL(sql)
		  //KMLからeNB-LCIDとピンの位置情報を抜いてSQLiteデータベースにかくぞーーーーーーー
		  sourcef=New FolderItem(kmlpath)
		  if sourcef.Exists then
		    kml.LoadXml(sourcef)
		    nodes = kml.XQL("//Placemark")
		    
		    //KML中の検出したPlaceMarkノードを0から検出数-1までなめる
		    Redim ENBLCIDList(nodes.Length-1)
		    For i as Integer = 0 To nodes.Length-1
		      node=nodes.item(i)
		      MatchPlacemark=Nil
		      CurrentENBLCID=""
		      
		      For c As Integer = 0 To node.ChildCount - 1
		        placemarkchild = node.Child(c)
		        Select Case placemarkchild.Name 
		        Case "name"
		          if placemarkchild.FirstChild <> nil then
		            MatchPlacemark=SearchPlacemark.Search(placemarkchild.FirstChild.value)
		            if MatchPlacemark<>nil then
		              CurrentENBLCID=MatchPlacemark.SubExpressionString(0)
		              dbrow.Column("""eNB-LCID""").StringValue = CurrentENBLCID
		              ENBLCIDList(i)=CurrentENBLCID
		              //msgbox("Name Found: "+CurrentENBLCID)
		            end if
		          end if
		        Case "ExtendedData"
		          For ExData as Integer = 0 To placemarkchild.ChildCount-1
		            if placemarkchild.Child(ExData).FirstChild.FirstChild <> nil then
		              //msgbox(ExData.ToString+"/"+placemarkchild.ChildCount.ToString+": "+placemarkchild.Child(ExData).FirstChild.FirstChild.value)
		              MatchPlacemark=SearchPlacemark.Search(placemarkchild.Child(ExData).FirstChild.FirstChild.value)
		              if MatchPlacemark<>nil then
		                CurrentENBLCID=MatchPlacemark.SubExpressionString(0)
		                dbrow.Column("""eNB-LCID""").StringValue = CurrentENBLCID
		                ENBLCIDList(i)=CurrentENBLCID
		                //msgbox("ExtendedData Found: "+CurrentENBLCID)
		              end if
		            end if
		          Next
		        Case "Point"
		          For PointChildCount As Integer = 0 To placemarkchild.ChildCount - 1
		            pointchild = placemarkchild.child(PointChildCount)
		            Select Case pointchild.Name
		            Case "coordinates"
		              Matchcoordinates=Searchcoordinates.Search(pointchild.FirstChild.value)
		              CurrentLongitude=Matchcoordinates.SubExpressionString(1)
		              CurrentLatitude=Matchcoordinates.SubExpressionString(2)
		              if Matchcoordinates<>nil then
		                dbrow.Column("""latitude""").StringValue = CurrentLatitude
		                dbrow.Column("""longitude""").StringValue = CurrentLongitude
		              end if
		            End Select
		          Next
		        End Select
		        
		      Next
		      App.DBFile.AddRow("DataTable", dbrow)
		      //TestWindow.raw.Text=TestWindow.raw.Text+CurrentENBLCID+","+CurrentLatitude+","+CurrentLongitude+Chr(13)+Chr(10)
		    Next
		    
		  end if
		  
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		ENBLCIDList() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PreviewedFileCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ProcessedFileCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ProcessedList(0,6) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TotalFileCount As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="TotalFileCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ProcessedFileCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="PreviewedFileCount"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
