#tag Class
Protected Class OrganizeMain
Inherits Thread
	#tag Event
		Sub Run()
		  FileSearcher(photopath)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub FileMover(f as FolderItem, DateTime as String, byref NearENBLCID as String, byref fileresult as String)
		  Dim copydir as FolderItem
		  
		  Dim d as DateTime
		  Dim md5 as String
		  Dim originalfilename,replacefilename as String
		  
		  //eNB-LCID名でフォルダーを作成する is True
		  if eNBLCIDDirMake then
		    //コンマをハイフンに置き換える(e.x. 123456-1-2-3) is True
		    if eNBLCIDDirNameNotComma then
		      if not exportpath.Child(NearENBLCID.ReplaceAll(",", "-")).Exists then
		        exportpath.Child(NearENBLCID.ReplaceAll(",", "-")).CreateFolder
		      end if
		      copydir=exportpath.Child(NearENBLCID.ReplaceAll(",", "-"))
		    else
		      //コンマをハイフンに置き換える(e.x. 123456-1-2-3) is False
		      if not exportpath.Child(NearENBLCID).Exists then
		        exportpath.Child(NearENBLCID).CreateFolder
		      end if
		      
		      //copydir = 上記で作成したフォルダー
		      copydir=exportpath.Child(NearENBLCID)
		    end if
		  else
		    //copydir = 自動分類した画像ファイルの保存先フォルダー
		    copydir=exportpath
		  end if
		  
		  //ファイルをコピー
		  if not copydir.Child(f.Name).Exists then
		    f.CopyTo copydir
		    
		    //ファイル名を変更する is True の場合の処理
		    if copydir.Child(f.Name)<>nil then
		      if copydir.Child(f.Name).Exists then
		        fileresult="コピー完了"
		        
		        if ChangeFileName=True and FileNameFormat<>"" then
		          //ファイル名フォーマットのテキストに%md5が含まれている場合だけmd5ハッシュを取得
		          if FileNameFormat.IndexOf("md5") <> -1 then md5=GetMD5(f)
		          
		          d=d.FromString(DateTime,Locale.Current,TimeZone.Current)
		          //%originalが含まれているかもしれないのでoriginalfilenameに現在のファイル名を退避
		          originalfilename=copydir.Child(f.Name).Name
		          replacefilename=FileNameFormat
		          //各フォーマットごとに置換実行
		          replacefilename=replacefilename.ReplaceAll("%date", format(d.Year,"00")+format(d.Month,"00")+format(d.Day,"00"))
		          replacefilename=replacefilename.ReplaceAll("%time", format(d.Hour,"00")+format(d.Minute,"00")+format(d.Second,"00"))
		          replacefilename=replacefilename.ReplaceAll("%original", originalfilename.ReplaceAll(".jpg", "").ReplaceAll(".JPG", ""))
		          replacefilename=replacefilename.ReplaceAll("%enblcid", NearENBLCID.ReplaceAll(",", "-"))
		          replacefilename=replacefilename.ReplaceAll("%number", copydir.Count.ToString)
		          replacefilename=replacefilename.ReplaceAll("%md5", md5)
		          
		          //ファイル名を変更
		          copydir.Child(f.Name).Name = replacefilename+".jpg"
		        end if
		      end if
		    end if
		  else
		    fileresult="同名ファイルあり"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FileSearcher(photopath as FolderItem)
		  TotalFileCount=photopath.Count
		  //ProcessedList = Array("","")
		  Redim ProcessedList(TotalFileCount, 6)
		  ProcessedFileCount=0
		  
		  Dim NearENBLCID As String
		  Dim NearENBLCIDDistance As String
		  Dim DateTime,Latitude,Longitude as String
		  Dim fileresult as String
		  
		  For c as Integer = 0 to photopath.Count-1
		    
		    if photopath.ChildAt(c).Name.IndexOf(".jpg") <>-1 then
		      ProcessedList(c, 0)=photopath.ChildAt(c).Name
		      
		      PhotoAnalyzer(photopath.ChildAt(c),NearENBLCID,NearENBLCIDDistance,DateTime,Latitude,Longitude,fileresult)
		      
		      ProcessedList(c, 1)=DateTime
		      ProcessedList(c, 2)=Latitude
		      ProcessedList(c, 3)=Longitude
		      ProcessedList(c, 4)=NearENBLCID
		      ProcessedList(c, 5)=NearENBLCIDDistance
		      ProcessedList(c, 6)=fileresult
		    end if
		    
		    ProcessedFileCount=ProcessedFileCount+1
		    
		    DateTime=""
		    Latitude=""
		    Longitude=""
		    NearENBLCID=""
		    NearENBLCIDDistance=""
		    
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GeoDistance(lat1 as Double, lng1 as Double, lat2 as Double, lng2 as Double) As Double
		  Dim P1,P2,A,B,F,X,L,distance,decimal_no,decimal as double
		  
		  //KanayamaSta
		  //lat1 = 35.142926
		  //lng1 = 136.8990095
		  
		  //Tekitouna-basho
		  //lat2 = 35.143953055134325
		  //lng2 = 136.90010674010634
		  
		  decimal = 14
		  
		  if (abs(lat1-lat2) < 0.00001) and (abs(lng1-lng2) < 0.00001) then
		    distance = 0
		  else
		    lat1=lat1*3.1415926535898/180
		    lng1=lng1*3.1415926535898/180
		    lat2=lat2*3.1415926535898/180
		    lng2=lng2*3.1415926535898/180
		    
		    A=6378140
		    B=6356755
		    F=(A-B)/A
		    
		    P1=atan((B/A)*tan(lat1))
		    P2=atan((B/A)*tan(lat2))
		    
		    X = acos(sin(P1)*sin(P2) + cos(P1)*cos(P2)*cos(lng1-lng2))
		    L = (F/8)*((sin(X)-X)*pow((sin(P1)+sin(P2)),2)/pow(cos(X/2),2)-(sin(X)-X)*pow(sin(P1)-sin(P2),2)/pow(sin(X),2))
		    
		    distance = A*(X+L)
		    decimal_no = pow(10,decimal)
		    distance = round(decimal_no*distance/1000)/decimal_no
		    return distance
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetMD5(f as FolderItem) As String
		  dim mdfive as new MD5Digest
		  dim bs as BinaryStream = BinaryStream.Open( f )
		  while not bs.EOF
		    //dim chunk as string = bs.Read( 1000000 )
		    dim chunk as string = bs.Read( bs.Length )
		    mdfive.Process chunk
		  wend
		  
		  dim hash as string = EncodeHex( mdfive.Value )
		  return hash
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetMetaData(source as FolderItem, byref DateTime as String, byref Latitude as String, byref Longitude as String)
		  Var desc As String
		  Dim SearchObject as New RegEx
		  Dim MatchObject as RegExMatch
		  
		  #If TargetMacOS
		    
		    // Declares against the "Foundation" framework
		    Declare Function NSClassFromString Lib "Foundation" (clsName As CFStringRef) As Ptr
		    Declare Function FileURLWithPath Lib "Foundation" Selector "fileURLWithPath:" (obj As Ptr, path As CFStringRef) As Ptr
		    Declare Function Description Lib "Foundation" Selector "description" (dict As Ptr) As CFStringRef
		    
		    // Declares against the "ImageIO" framework
		    Declare Function CGImageSourceCreateWithURL Lib "ImageIO" (path As Ptr, options As Ptr) As Ptr
		    Declare Function CGImageSourceCopyPropertiesAtIndex Lib "ImageIO" (imageSource As Ptr, index As Integer, options As Ptr) As Ptr
		    
		    If source <> Nil Then
		      
		      // Getting a Reference to the NSURL class
		      Var nsurl As Ptr = NSClassFromString("NSURL")
		      
		      // Getting a reference to a NSURL object created from the given Path
		      // for the source file
		      Var filePath As Ptr = FileURLWithPath(nsurl, source.NativePath)
		      
		      // Getting a reference to the image object created from the 
		      // given NSURL object
		      Var imageRef As Ptr = CGImageSourceCreateWithURL(filePath, Nil)
		      
		      // Getting an NSDictionary containing the Metadata for the image
		      Var dict As Ptr = CGImageSourceCopyPropertiesAtIndex(imageRef, 0, Nil)
		      
		      // Getting the contents of the NSDictionary instance as a String
		      // This will contain the metadata for the image file!
		      desc = Description(dict)
		      
		    End If
		    
		  #Elseif TargetWindows
		    Dim s as new shell
		    
		    //Dim WinTemp,WinDateTimeOriginal,WinGPSLatitude,WinGPSLongitude as String
		    
		    
		    Dim powershellcmd as string
		    dim c as TextConverter
		    c=GetTextConverter(Encodings.ShiftJIS, Encodings.UTF8)
		    powershellcmd = "powershell.exe -NoProfile -ExecutionPolicy Bypass """
		    powershellcmd = powershellcmd + "$image = New-Object -ComObject Wia.ImageFile ;"
		    powershellcmd = powershellcmd + "$image.loadfile(\"""+ source.NativePath + "\"") ;"
		    powershellcmd = powershellcmd + "$xdt = [string]$image.Properties.Item(\""DateTime\"").Value ;"
		    powershellcmd = powershellcmd + "$xlat = $image.Properties.Item(\""GpsLatitude\"").Value[1].Value + $image.Properties.Item(\""GpsLatitude\"").Value[2].Value/60 + $image.Properties.Item(\""GpsLatitude\"").Value[3].Value/3600; "
		    powershellcmd = powershellcmd + "$xlng = $image.Properties.Item(\""GpsLongitude\"").Value[1].Value + $image.Properties.Item(\""GpsLongitude\"").Value[2].Value/60 + $image.Properties.Item(\""GpsLongitude\"").Value[3].Value/3600; "
		    powershellcmd = powershellcmd + "[string]\""DateTimeOriginal = \""\""$xdt\""\"";\"";"
		    powershellcmd = powershellcmd + "[string]\""Latitude = \""\""$xlat\""\"";\"";"
		    powershellcmd = powershellcmd + "[string]\""Longitude = \""\""$xlng\""\"";\"";"
		    powershellcmd = powershellcmd + ""
		    s.Execute(powershellcmd)
		    desc= c.Convert(s.Result)
		    
		    's.Execute("$image = New-Object -ComObject Wia.ImageFile")
		    's.Execute("$image.loadfile("""+ source.NativePath + """)")
		    '
		    'WinDateTimeOriginal = "DateTimeOriginal = """ + s.Execute("[string]$image.Properties.Item(""DateTime"").Value") + """
		    'WinGPSLatitude = "Latitude = """ + s.Execute("[string]$image.Properties.Item(""GpsLatitude"").Value[1].Numerator + "." + [string]$image.Properties.Item(""GpsLatitude"").Value[2].Numerator + [string]$image.Properties.Item(""GpsLatitude"").Value[3].Numerator") + """
		    'WinGPSLongitude = "Longitude = """ + s.Execute("[string]$image.Properties.Item(""GpsLongitude"").Value[1].Numerator + "." + [string]$image.Properties.Item(""GpsLongitude"").Value[2].Numerator + [string]$image.Properties.Item(""GpsLongitude"").Value[3].Numerator") + """
		    
		    //desc = WinDateTimeOriginal + Chr(13) + WinGPSLatitude + Chr(13) + WinGPSLongitude
		  #EndIf
		  
		  //DateTimeOriginal = "2022:06:19 17:56:12";
		  SearchObject.SearchPattern="DateTimeOriginal = ""(\d{4}).(\d{2}).(\d{2}).(\d{2}).(\d{2}).(\d{2})"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    DateTime=MatchObject.SubExpressionString(1)+"/"+MatchObject.SubExpressionString(2)+"/"+MatchObject.SubExpressionString(3)+" "+MatchObject.SubExpressionString(4)+":"+MatchObject.SubExpressionString(5)+":"+MatchObject.SubExpressionString(6)
		  else
		    DateTime="2000/01/01 00:00:00"
		  end if
		  
		  //Latitude = "34.99706666666667";
		  SearchObject.SearchPattern="Latitude = ""(.*)"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    Latitude=MatchObject.SubExpressionString(1)
		  end if
		  
		  //Longitude = "137.0679333333333";
		  SearchObject.SearchPattern="Longitude = ""(.*)"";"
		  MatchObject=SearchObject.Search(desc)
		  if MatchObject<>nil then
		    Longitude=MatchObject.SubExpressionString(1)
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PhotoAnalyzer(f as FolderItem, byref NearENBLCID as String, byref NearENBLCIDDistance as String, byref DateTime as String, byref Latitude as String, byref Longitude as String, byref fileresult as String)
		  Dim Distance as Double
		  
		  Dim eNBLCIDRowSet As RowSet
		  Dim eNBLCIDLatitude,eNBLCIDLongitude As String
		  
		  Dim CalcRowSet As RowSet
		  
		  Dim dbrow as New DatabaseRow
		  
		  Dim sql as string
		  
		  sql = "DROP TABLE IF EXISTS CALC;"
		  App.DBFile.ExecuteSQL(sql)
		  
		  sql = "CREATE TABLE CALC (""eNB-LCID"" TEXT, ""DISTANCE"" REAL);"
		  App.DBFile.ExecuteSQL(sql)
		  
		  NearENBLCID = ""
		  NearENBLCIDDistance = ""
		  if f<>nil then
		    if f.Exists then
		      GetMetaData(f,DateTime,Latitude,Longitude)
		      
		      for c as integer = 0 to ENBLCIDList.Count-1
		        eNBLCIDRowSet = App.DBFile.SelectSQL("SELECT * from DataTable where `eNB-LCID` = '" + ENBLCIDList(c) + "';")
		        
		        if eNBLCIDRowSet.RowCount > 0 then 
		          eNBLCIDLatitude = eNBLCIDRowSet.Column("latitude").StringValue
		          eNBLCIDLongitude = eNBLCIDRowSet.Column("longitude").StringValue
		          
		          //km * 1000 = m
		          Distance=Round(GeoDistance(Latitude.ToDouble,Longitude.ToDouble,eNBLCIDLatitude.ToDouble,eNBLCIDLongitude.ToDouble)*1000)
		          
		          //TestWindow.raw.Text=TestWindow.raw.Text+""""+ENBLCIDList(c)+""""+","+Distance.ToString+","+eNBLCIDLatitude+","+eNBLCIDLongitude+Chr(13)+Chr(10)
		          
		          dbrow.Column("""eNB-LCID""").StringValue = ENBLCIDList(c)
		          dbrow.Column("""DISTANCE""").StringValue = Distance.ToString
		          App.DBFile.AddRow("CALC", dbrow)
		        end if
		      next
		      
		      CalcRowSet = App.DBFile.SelectSQL("SELECT * from CALC ORDER BY DISTANCE ASC LIMIT 1;")
		      
		      if CalcRowSet.RowCount > 0 then 
		        NearENBLCID = CalcRowSet.Column("eNB-LCID").StringValue
		        NearENBLCIDDistance = CalcRowSet.Column("DISTANCE").StringValue
		        
		        if NearENBLCIDDistance.ToInteger <= Tolerance then
		          FileMover(f,DateTime,NearENBLCID,fileresult)
		        else
		          fileresult="基地局周辺範囲外"
		        end if
		      else
		        fileresult="GPS情報なし"
		      end if
		      
		    end if
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		ChangeFileName As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		eNBLCIDDirMake As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		eNBLCIDDirNameNotComma As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		exportpath As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		FileNameFormat As String
	#tag EndProperty

	#tag Property, Flags = &h0
		photopath As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Tolerance As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			InitialValue=""
			Type="String"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="Priority"
			Visible=true
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="StackSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="DebugIdentifier"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThreadID"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ThreadState"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="ThreadStates"
			EditorType="Enum"
			#tag EnumValues
				"0 - Running"
				"1 - Waiting"
				"2 - Paused"
				"3 - Sleeping"
				"4 - NotRunning"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Tolerance"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Integer"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="eNBLCIDDirMake"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Boolean"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="eNBLCIDDirNameNotComma"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Boolean"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="ChangeFileName"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="Boolean"
			EditorType=""
		#tag EndViewProperty
		#tag ViewProperty
			Name="FileNameFormat"
			Visible=false
			Group="Behavior"
			InitialValue=""
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
